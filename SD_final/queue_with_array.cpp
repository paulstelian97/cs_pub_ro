#include <iostream>
using namespace std;

template<typename T> class Queue {
    private:
        T* container;
        int head,tail;
        int maxSize;

    public:
        Queue(int maxSize) {
            this->maxSize = maxSize+1; // this or additional variable is required
            container = new T[maxSize+1]; // to distinguish empty from full
            // attempting to find another solution though
            head = tail = 0;
        }

        ~Queue() {
            delete[] container;
        }

        void enqueue(T x) {
            //TODO: completati functia de adaugare in coada
            if (((tail+1) % maxSize) == head)
                cout << "ERR: Coada e plina" << endl;
            else
                {
                container[tail++] = x;
                tail = tail % maxSize;
            }
        }

        T dequeue() {
            //TODO: completati functia de extragere din coada
            if (isEmpty()) {
                cout << "ERR: Coada e goala" << endl;
                return T();
            }
            int oldhead = head++;
            head = head % maxSize;
            return container[oldhead];
        }

        T peek() const {
            if (isEmpty()){
              cout<<"ERR: Coada e goala"<<endl;
              return T();
            }
            return container[head];
        }

        bool isEmpty() const {
            return head==tail;
        }
};

class Calator {
    private:
      string name;
    public:
      Calator() {}
      Calator(string name) {
        this->name = name;
      }
      string getName() {
        return name;
      }
};

int main() {

    int queueSize, nrCalatori;
    cin>>queueSize>>nrCalatori;

    Queue<Calator> coadaCasa = Queue<Calator>(queueSize);

    for(int i=0; i<nrCalatori; i++){
        coadaCasa.enqueue(Calator("Calator"+to_string(i)));
    }

    while (!coadaCasa.isEmpty()){
        cout<<coadaCasa.dequeue().getName()<<endl;
    }
 
    if (nrCalatori>=10) coadaCasa.dequeue();
}

