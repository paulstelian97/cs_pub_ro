#include <iostream>
#include <vector>
#include <queue>
#include <stack>
#include <string>
#include <climits>

class Node {
private:
   unsigned int id;
   std::string  name;

   bool         visited;
   Node*        parent;

public:
   Node(unsigned int id) :
      id(id), name("unknown"), visited(false), parent(NULL) {}
   Node(unsigned int id, std::string name) :
      id(id), name(name),      visited(false), parent(NULL) {}

   const unsigned int get_id()     const        { return id; }
   const std::string& get_name()   const        { return name; }
   void               set_name(std::string str) { name = str; }

   void               visit()                   { visited = true; }
   bool               is_visited() const        { return visited; }

   Node*              get_parent() const        { return parent; }
   void               set_parent(Node* p)       { parent = p; }

   void               reset()                   { visited = false; parent = NULL; }
};

class Edge {
private:
   Node* target;
   int   cost;

public:
   Edge(Node* target)           : target(target), cost(1)    {}
   Edge(Node* target, int cost) : target(target), cost(cost) {}

   Node* get_target() const  { return target; }
   void  set_target(Node* n) { target = n; }

   int   get_cost()   const  { return cost; }
   void  set_cost(int c)     { cost = c; }
};

class Graph {
private:
   std::vector<Node*>               nodes;
   std::vector<std::vector<Edge*> > edges;

public:
   Graph() {}
   ~Graph()
    {
      for (unsigned int i = 0; i < nodes.size(); i++)
         delete nodes[i];

      for (unsigned int i = 0; i < edges.size(); i++)
         for (unsigned int j = 0; j < edges[i].size(); j++)
            delete edges[i][j];
   }

   void insert_node(Node* node)
   {
      nodes.push_back(node);
      edges.push_back(std::vector<Edge*>());
   }

   Node*               get_node(unsigned int id) const { return (id < nodes.size()) ? nodes[id] : NULL; }
   std::vector<Node*>& get_nodes()                     { return nodes; }
   unsigned int        get_node_count()          const { return nodes.size(); }

   void insert_edge(Node* node1, Node* node2, int cost)
   {
      if (node1->get_id() < edges.size())
         edges[node1->get_id()].push_back(new Edge(node2, cost));
   }

   std::vector<Edge*>& get_edges(Node* node)           { return edges[node->get_id()]; }
};

void read(Graph& graph, unsigned int& start, unsigned int& end)
{
   unsigned int nr_nodes, nr_edges;

   std::cin >> nr_nodes >> nr_edges >> start >> end;
   for (unsigned int i = 0; i < nr_nodes; i++) {
      unsigned int id;
      std::string name;

      std::cin >> id >> name;
      graph.insert_node(new Node(id, name));
   }

   std::vector<Node*> nodes = graph.get_nodes();
   for (unsigned int i = 0; i < nr_edges; i++) {
      unsigned int id1, id2;
      int cost;

      std::cin >> id1 >> id2 >> cost;
      graph.insert_edge(nodes[id1], nodes[id2], cost);
      graph.insert_edge(nodes[id2], nodes[id1], cost);
   }
}

void print(std::vector< Node * > path) {
    if (path.empty()) return;

   std::cout << path[0]->get_name() << " -> " << path[path.size() - 1]->get_name() << " : ";
   for (unsigned int i = 0; i < path.size() - 1; i++)
      std::cout << path[i]->get_name() << ", ";
   std::cout << path[path.size() - 1]->get_name() << "\n";
}

std::vector<Node*> build_path(Node* node)
{
   std::vector<Node*> path;
   std::stack<Node*> nstack;

   nstack.push(node);
   while (node->get_parent()) {
      nstack.push(node->get_parent());
      node = node->get_parent();
   }

   while (!nstack.empty()) {
      path.push_back(nstack.top());
      nstack.pop();
   }

   return path;
}

std::vector<Node*> bfs(Graph& graph, Node* start, Node* end)
{
    // Nice Dijkstra here?
   // initializati vectorul de distante asociat nodului sursa (toate nodurile au distanta infinit -> vezi INT_MAX)
    std::vector<int> distances(graph.get_node_count(), INT_MAX);
    
   // initializati distanta fata de sursa cu 0
    distances[start->get_id()] = 0;

   // creati o structura de date care sa va ajute in parcurgerea BFS
    std::queue<int> for_bfs;
   // adaugati nodul sursa
    for_bfs.push(start->get_id());

   // cat timp mai sunt noduri de parcurs
    while (!for_bfs.empty()) {
      // scot un nod din structura si il marchez ca vizitat
        int node = for_bfs.front();
        for_bfs.pop();
        graph.get_node(node)->visit();
      // pentru fiecare vecin (muchie)
        for (auto& e : graph.get_edges(graph.get_node(node))) {
         // daca distanta[vecin] > distanta[curent] + cost muchie
            if (distances[e->get_target()->get_id()] > distances[node] + e->get_cost()) {
            // actualizez distanta[vecin] cu distanta[curent] + cost muchie
                distances[e->get_target()->get_id()] = distances[node] + e->get_cost();
            // actualizez parintele vecinului cu nod curent (drumul prin nod curent catre vecin e mai scurt)
                e->get_target()->set_parent(graph.get_node(node));
            }
         // daca nodul vecin nu a fost vizitat
            if (!e->get_target()->is_visited())
            // adauga vecin in structura
                for_bfs.push(e->get_target()->get_id());
        }
    }
   // intoarce calea de la ultimul nod (end) -> vezi build_path
    return build_path(end);

   return std::vector<Node*>(); // delete me when you are done!
}

int main(void)
{
   Graph g;
   unsigned int start, end;

   read(g, start, end);
   print(bfs(g, g.get_node(start), g.get_node(end)));

   return 0;
}


