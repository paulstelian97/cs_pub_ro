#include <iostream>
#include <string>

using namespace std;

template<typename T> struct list_elem {
    T info;
    struct list_elem<T> *next;
};

template <typename T> class LinkedList {
    public:
    struct list_elem<T> *plast;

    LinkedList() {
        plast = NULL;
    }

    void addFirst(T x) {
        struct list_elem<T> *paux;
        paux = new struct list_elem<T>;
        paux->info = x;
        if (plast == NULL) {
            plast = paux;
            plast->next = plast;
        } else {
            paux->next = plast->next;
            plast->next = paux;
        }
    }

    void addLast(T x) {
        //TODO: completati operatia de adaugare la final
        if (!plast) return addFirst(x); // if empty, first and last are the same
        list_elem<T>* new_node = new list_elem<T>();
        new_node->info = x;
        new_node->next = plast -> next;
        plast -> next = new_node;
        plast = new_node;
    }

    T removeFirst() {
        //TODO: completati operatia de extragere de la inceput
        if (!plast) {
            cout << "Error 101 - The list is empty\n";
            return T();
        }
        list_elem<T>* pFirst = plast->next;
        T rez(pFirst->info);
        plast->next = pFirst->next;
        if (plast == pFirst) // last element
            plast = NULL;
        delete pFirst;
        return rez;
    }

    T removeLast() {
        T rez;
        struct list_elem<T>* paux;
        if (plast!=NULL) { // list not empty
            rez = plast->info;
            paux = plast;
            if (plast->next!=plast) { // more than one element in queue
                while (plast->next!=paux) plast = plast->next;
                plast->next = paux->next;
            } else {
                plast = NULL;
            }
            delete paux;
        }
        else cout<<"Error 101 - The list is empty\n";
        return rez;
    }

    int size() {
        int cnt = 0;
        if (plast!=NULL) {
            struct list_elem<T> *paux = plast->next;
            cnt++;
            while (paux != plast) {
                paux = paux->next;
                cnt++;
            }
        }
        return cnt;
    }

};

class Calator {
    private:
      string name;
    public:
      Calator() {}
      Calator(string name) {
        this->name = name;
      }
      string getName() {
        return name;
      }
};

int main() {

    int nrCalatori;
    cin>>nrCalatori;

    LinkedList<Calator> coadaCasa = LinkedList<Calator>();

    for(int i=0; i<nrCalatori; i++){
        coadaCasa.addLast(Calator("Calator"+to_string(i)));
    }

    cout<<coadaCasa.size()<<endl;

    if (nrCalatori>=5) {
      while (coadaCasa.size()>0){
          cout<<coadaCasa.removeFirst().getName()<<endl;
      }
    }

    if (nrCalatori>=10) coadaCasa.removeFirst();
}
