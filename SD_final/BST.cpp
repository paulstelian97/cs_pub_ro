#include <iostream>
#include <vector>
#include <string.h> // memcpy

using namespace std;

template <typename T>
class BinarySearchTree
{
private:
    BinarySearchTree<T> *branchLeft;
    BinarySearchTree<T> *branchRight;

    T *value;

public:

  BinarySearchTree(): branchLeft(NULL), branchRight(NULL), value(NULL) {}
  BinarySearchTree(T data): branchLeft(NULL), branchRight(NULL) {
    setData(data);
  }

  void setData(T data) {
    value = (T *)new char[sizeof(T)]; // not calling constructor lol
    memcpy(value, &data, sizeof(data));
  }

  ~BinarySearchTree() {
    if (value)
      delete value;

    if (branchLeft)
      delete branchLeft;

    if (branchRight)
      delete branchRight;
  }

  void insertKey(T key) {
    // TODO: complete the insertKey(T key) method
      if (!value) {
          setData(key);
          branchLeft = new BinarySearchTree();
          branchRight = new BinarySearchTree(); // yes, let's have empty nodes!
      }
      else
          {
          if (*value >= key)
              branchLeft->insertKey(key);
          else
              branchRight->insertKey(key);
      }
  }

  bool searchKey(T key) {
   // TODO: complete the searchKey(T key) method
      if (!value) return false;
      if (*value == key) return true;
      if (key < *value)
          return branchLeft->searchKey(key);
      return branchRight->searchKey(key);
  } 

  void printInOrder() {
    // TODO: complete the printInOrder() method
      if (!value) return;
      branchLeft->printInOrder();
      cout << *value << ' ';
      branchRight->printInOrder();
  }

};

void checkKey(BinarySearchTree<int>* bst, int key) {
  cout << "Cheia: " << key << (bst->searchKey(key) ? " " : " nu ") <<
    "se afla in arbore" << endl;
}

int main() {
  BinarySearchTree<int>* bst = new BinarySearchTree<int>;

  int n;
  cin >> n;

  vector<int> vct(n);
  for(int i = 0; i < n; i++) {
    cin >> vct[i];
  }

  for(int i = 0; i < n; i++) {
    bst->insertKey(vct[i]);
  }
  
  int testCase = -1;
  cin >> testCase;
  
  switch(testCase) {
    case 1:
      checkKey(bst, 2);
      checkKey(bst, 18);
      checkKey(bst, 12);
      checkKey(bst, 38);
      checkKey(bst, 11);
      checkKey(bst, 115);
      checkKey(bst, 114);
      checkKey(bst, -3);
      checkKey(bst, 22);
      checkKey(bst, 20);
      checkKey(bst, 1);
      checkKey(bst, 4);
      checkKey(bst, 13);
      checkKey(bst, 17);
      checkKey(bst, 23);
      break;
    case 2:
      bst->printInOrder();
      cout << endl;
      break;
    default:
      return -1;
  }
  
  return 0;
}
