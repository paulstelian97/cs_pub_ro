#include <iostream>
#include <queue>
#include <stack>
#include "graph.h"
bool disp_bfs(const Graph& g, int from, int to);
int main() {
    using namespace std;
    int n, m;
    cin >> n >> m;
    Graph towns(n);
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        towns.put(a, b, 1, true);
    }
    int A, B;
    cin >> A >> B;
    if (!disp_bfs(towns, A, B))
        cout << "Impossible" << endl;
}
bool disp_bfs(const Graph& g, int from, int to) {
    int nodes;
    int* v = new int[nodes = g.get_nodecount()];
    bool result = false;
    for (int i = 0; i < nodes; i++)
        v[i] = -1;
    std::queue<std::pair<int, int> > q;
    q.push(std::make_pair(from, 0));
    while (!q.empty() && !result) {
        std::pair<int, int> cur_node = q.front();
        q.pop();
        if (v[cur_node.first] == -1)
            v[cur_node.first] = cur_node.second;
        for (int i = 0; i < nodes; i++) {
            if (v[i] != -1) continue; //skip already hit
            if (!g.get(cur_node.first, i)) continue; //skip those not imm. reachable
            q.push(std::make_pair(i, cur_node.second + 1));
        }
        if (cur_node.first == to) result = true;
    }
    if (result) {
        // We have a path; let's find it
        std::stack<int> path;
        while (to != from) {
            path.push(to);
            for (int i = 0; i < nodes; i++)
                if (v[i] + 1 == v[to] && g.get(i, to)) {
                    to = i;
                    break;
                }
        }
        std::cout << from;
        while (!path.empty()) {
            int elem = path.top();
            path.pop();
            std::cout << ' ' << elem;
        }
    }
    delete[] v;
    return result;
}
