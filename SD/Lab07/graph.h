#pragma once
#include <cstring>
class Graph {
    int *M;
    int nodes;
    Graph(const Graph&);
    Graph& operator=(const Graph&);
 public:
    Graph(int new_nodes)
        : nodes(new_nodes), M(new int[new_nodes * new_nodes]) 
    {
        memset(M, 0, nodes * nodes * sizeof(int));
    }
    void put(int left, int right, int val = 1, bool both = false) {
        if (left < 0 || right < 0 || left >= nodes || right >= nodes) return;
        M[left * nodes + right] = val;
        if (both) M[right * nodes + left] = val;
    }
    int get(int left, int right) const {
        if (left < 0 || right < 0 || left >= nodes || right >= nodes) return 0;
        return M[left * nodes + right];
    }
    int get_nodecount() const { return nodes; }
    ~Graph() { delete[] M; }
};
