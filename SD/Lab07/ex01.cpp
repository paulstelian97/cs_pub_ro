#include <iostream>
#include <cstring>
#include "graph.h"
bool disp = false;
int dfs_launch(const Graph& g);
int main() {
    using namespace std;
    int n, m;
    cin >> n >> m;
    Graph g(n);
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        g.put(a, b, 1, true);
    }
    cout << dfs_launch(g) << endl;
    disp = true;
    dfs_launch(g);
}
int dfs(const Graph& g, int* vhit, int nodes, int curr_node, bool start = 0) {
    if (curr_node < 0 || curr_node >= nodes) return 0;
    if (vhit[curr_node]) return 0;
    if (disp)
        std::cout << curr_node << ' ';
    vhit[curr_node] = 1;
    for (int i = 0; i < nodes; i++) {
        if (vhit[i]) continue; // skip those already hit
        if (g.get(curr_node, i))
            dfs(g, vhit, nodes, i);
    }
    if (start && disp) std::cout << std::endl;
    return 1;
}
int dfs_launch(const Graph& g) {
    int nodes = g.get_nodecount();
    int* vhit = new int[nodes];
    memset(vhit, 0, nodes * sizeof(int));
    int result = 0;
    for (int i = 0; i < nodes; i++)
        result += dfs(g, vhit, nodes, i, true);
    delete[] vhit;
    return result;
}
