/**
  * Autor: Victor Carbune
  * Echipa SD, 2012
  *
  * Modificari: Adrian Bogatu
  * Echipa SD, 2013
  *
  * Modificari: Cosmin Petrisor
  * Echipa SD, 2015
  *
  * License: LGPL
  */

#ifndef __BINARY_TREE_H
#define __BINARY_TREE_H

#include <iostream>
#include <cstdlib>

using namespace std;

template <typename T>
class BinaryTree
{
private:

    T *pData;

    BinaryTree<T> *leftNode;
    BinaryTree<T> *rightNode;

public:

    //constructor TODO 1.1
    BinaryTree() {
        leftNode = rightNode = NULL;
        pData = NULL;
    }

    //destructor TODO 1.1
    // free up data for pData, left and right node
    ~BinaryTree() {
        delete pData;
        delete leftNode;
        delete rightNode;
    }

    // Inspect if the current node has data. TODO 1.2
    bool hasData() { return pData != NULL;}

    // Set data for current node. TODO 1.2
    void setData(T data) {
        delete pData;
        pData = new T(data); // use copy constructor
    }

    // Get current node's data. TODO 1.2
    T getData() {
        if (!pData) return T();
        return *pData;
    }

    // Set left subtree. TODO 1.2
    void setLeftSubtree(BinaryTree<T> *node) {
        delete leftNode;
        leftNode = node;
    }

    // Get the left subtree. TODO 1.2
    BinaryTree<T> *getLeftSubtree() { return leftNode; }

    // Set right subtree. TODO 1.2
    void setRightSubtree(BinaryTree<T> *node) {
        delete rightNode;
        rightNode = node;
    }
    // Get the right subtree. TODO 1.2
    BinaryTree<T> *getRightSubtree() { return rightNode; }

    // Insert data recursively randomly where it is possible. TODO 1.3
    // compute a random value: 0 or 1.
    // http://www.cplusplus.com/reference/cstdlib/rand/
    // if 0 insert in left subtree like so:
    //   - if left subtree has data, then call insert recursively on it
    //   - else insert data into left node
    // else insert in right following exact same logic
    #warning RANDOM NOT DONE
    void insertRandomRecursively(T data) {
        if (rand() % 2) {
            if (rightNode)
                rightNode->insertRandomRecursively(data);
            else
                insertRight(data);
        } else {
            if (leftNode)
                leftNode->insertRandomRecursively(data);
            else
                insertLeft(data);
        }
    }

    // Insert data into left subtree. TODO 1.3
    // If left node doesn't exist then allocate memory for it.
    // After that, set data on it.
    void insertLeft(T data) {
        if (!leftNode)
            leftNode = new BinaryTree<T>();
        leftNode->setData(data);
    }

    // Insert data into right subtree. TODO 1.3
    // If right node doesn't exist then allocate memory for it.
    // After that, set data on it.
    void insertRight(T data) {
        if (!rightNode)
            rightNode = new BinaryTree<T>();
        rightNode->setData(data);
    }

    // Display the binary tree to output. TODO 2.1
    void displayTree(int indentLevel) {
        if (leftNode)
            leftNode->displayTree(indentLevel+1);
        for (int i = 0; i < indentLevel; i++) std::cout << ' ';
        std::cout << getData();
        std::cout << std::endl;
        if (rightNode)
            rightNode->displayTree(indentLevel+1);
    }

    // Return the height of the binary tree. TODO 2.2
    int getTreeHeight() {
        if (this == NULL) return 0; // Shouldn't happen though
        int subh = 0;
        if (leftNode && leftNode->getTreeHeight() > subh)
            subh = leftNode->getTreeHeight();
        if (rightNode && rightNode->getTreeHeight() > subh)
            subh = rightNode->getTreeHeight();
        return subh + 1;
    }

    // Return the number of nodes in the tree. TODO 2.3
    int getNumNodes() {
        if (this == NULL) return 0; // Shouldn't happen though
        int nodes = 1;
        if (leftNode)
            nodes += leftNode->getNumNodes();
        if (rightNode)
            nodes += rightNode->getNumNodes();
        return nodes;
    }
};


#endif // __BINARY_TREE_H
