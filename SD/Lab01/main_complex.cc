#include <iostream>
#include "complex.h"
using namespace std;

int main()
{
	Complex x(2,3), y(1,5);
	cout << "x=" << x.getRe() << "+" << x.getIm() << "i" << endl;
	cout << "y=" << y.getRe() << "+" << y.getIm() << "i" << endl;
	Complex sum = x.add(y), diff = x.sub(y), prod = x.mul(y);
	cout << "sum=" << sum.getRe() << " + " << sum.getIm() << "i" << endl;
	cout << "diff=" << diff.getRe() << " + " << diff.getIm() << "i" << endl;
	cout << "prod=" << prod.getRe() << " + " << prod.getIm() << "i" << endl;
}
