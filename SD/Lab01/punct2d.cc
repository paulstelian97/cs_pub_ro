#include <cmath>
#include "punct2d.h"
double Punct2D::dist(Punct2D other) {
	double result = (this->x - other.x) * (this->x - other.x);
	result += (this->y - other.y) * (this->y - other.y);
	result = sqrt(result);
	return result;
}
