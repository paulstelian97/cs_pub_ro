class Punct2D {
	double x, y;
public:
	Punct2D() {x=y=0;}
	Punct2D(double X, double Y) {this->x=X; this->y=Y;}
	double getX() {return x;}
	double getY() {return y;}
	Punct2D projX() {
		Punct2D result(0, this->y);
		return result;
	}
	Punct2D projY() {
		Punct2D result(this->x, 0);
		return result;
	}
	double dist(Punct2D other); //to be implemented in separate module
};
