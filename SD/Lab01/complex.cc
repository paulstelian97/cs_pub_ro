#include "complex.h"
Complex::Complex(double re, double im) {
    this->re = re;
    this->im = im;
}
 
Complex::~Complex() {
}
 
Complex Complex::conjugate() {
    Complex conjugat(re, -im);
    return conjugat;
}
 
double Complex::getRe() {
    return re;
}
 
double Complex::getIm() {
    return im;
}

Complex Complex::add(Complex other) {
	Complex result(this->re + other.re, this->im + other.im);
	return result;
}

Complex Complex::sub(Complex other) {
	Complex result(this->re - other.re, this->im - other.im);
	return result;
}

Complex Complex::mul(Complex other) {
	Complex result(
		this->re * other.re - this->im * other.im,
		this->re * other.im + this->im * other.re
	);
	return result;
}
