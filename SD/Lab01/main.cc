#include <iostream>
#include "punct2d.h"
using namespace std;
int main() {
	Punct2D A(2,3), B(1,4);
	cout << "A(" << A.getX() << "," << A.getY() << "), ";
	cout << "B(" << B.getX() << "," << B.getY() << ")" << endl << endl;
	cout << "Projection of A on Ox is at (" <<
		A.projX().getX() << "," << A.projX().getY() << ")" << endl;
	cout << "Projection of B on Oy is at (" <<
		B.projY().getX() << "," << B.projY().getY() << ")" << endl;
	cout << endl;
	cout << "Distance between A and B is " << A.dist(B) << endl;
}
