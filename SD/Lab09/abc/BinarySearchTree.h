#ifndef __BINARY_SEARCH_TREE__H
#define __BINARY_SEARCH_TREE__H

#include <iostream> 
#include <cassert>
 
template <typename T>
class BinarySearchTree
{
public:
    BinarySearchTree() {
        leftNode = NULL;
        rightNode = NULL;
        parent = NULL; // root
        pData = NULL;
    }
    ~BinarySearchTree() {
        // Destroy the tree!
        delete leftNode;
        delete rightNode;
        if (parent) {
            if (parent -> leftNode == this)
                parent -> leftNode = NULL;
            else
                parent -> rightNode = NULL;
        }
        delete pData; // NULL is safe
    }
    
    bool isEmpty() {
        return (pData == NULL);
    }
 
    void insertKey(T x) {
        if (!pData) {
            pData = new T(x);
            return;
        }
        if (*pData < x) {
            // it's in the right side
            if (rightNode)
                rightNode -> insertKey(x);
            else {
                // We must create node
                rightNode = new BinarySearchTree<T>();
                rightNode -> parent = this;
                rightNode -> insertKey(x); // would go to first case, which is gud!
            }
        } else {
            // it's in the left side
            if (leftNode)
                leftNode -> insertKey(x);
            else {
                // We must create node
                leftNode = new BinarySearchTree<T>();
                leftNode -> parent = this;
                leftNode -> insertKey(x); // would go to first case, which is gud!
            }
        }
    }
    
    BinarySearchTree<T>* searchKey(T x) {
        if (!pData)
            return NULL; // well I suppose tree is empty anyways
        if (*pData == x)
            return this;
        if (*pData < x && rightNode)
            return rightNode -> searchKey(x);
        if (leftNode)
            return leftNode -> searchKey(x);
        return NULL;
    }
    
    void inOrderDisplay() {
        if (leftNode)
            leftNode -> inOrderDisplay();
        if (pData)
            std::cout << *pData << ' ';
        if (rightNode)
            rightNode -> inOrderDisplay();
    }
    
    T findMin() {
        if (leftNode)
            return leftNode -> findMin();
        if (pData)
            return *pData;
        return T();
    }
    
    T findMax() {
        if (rightNode)
            return rightNode -> findMax();
        if (pData)
            return *pData;
        return T();
    }
    
    int findLevels() {
        int left_height = 0, right_height = 0;
        if (leftNode) left_height = leftNode -> findLevels();
        if (rightNode) right_height = rightNode -> findLevels();
        if (right_height > left_height) left_height = right_height;
        return left_height + 1;
    }
    
    void displayLevel(int level) {
        displayLevelReal(level);
        std::cout << std::endl;
    }
    
    BinarySearchTree<T>* removeKey(T x) {
        //TODO 3
        BinarySearchTree<T>* nodeToRemove = searchKey(x);
        if (!nodeToRemove) return this; // remove nothing
        // Woah, let's take a few cases
        if (!nodeToRemove->leftNode && !nodeToRemove->rightNode) {
            // Leaf nodes are easy to remove
            if (parent) {
                if (parent -> leftNode == this)
                    parent -> leftNode = NULL;
                else
                    parent -> rightNode = NULL;
            }
            delete nodeToRemove;
            // pointer comparisons are ok even if they are invalid
            if (nodeToRemove == this) return NULL; // ouch
            return this;
        }
        if (nodeToRemove == this) {
            // Find a replacement; it always exist
            delete pData; // remove old data
            pData = NULL;
            BinarySearchTree<T>* repl = NULL;
            T replval;
            if (rightNode) {
                replval = rightNode -> findMin();
                repl = rightNode -> searchKey(replval);
            }
            if (!repl) { // try on the left then
                replval = leftNode -> findMax();
                repl = leftNode -> searchKey(replval);
            }
            assert(repl);
            pData = new T(replval);
            if (leftNode)
                leftNode = leftNode -> removeKey(replval);
            if (rightNode)
                rightNode = rightNode -> removeKey(replval);
            return this;
        }
        BinarySearchTree** ptr_to_modify;
        if (nodeToRemove->parent->leftNode == nodeToRemove)
            ptr_to_modify = &(nodeToRemove->parent->leftNode);
        else
            ptr_to_modify = &(nodeToRemove->parent->rightNode);
        *ptr_to_modify = nodeToRemove -> removeKey(x); // fall to previous case
        return this;
    }
 
private:
    BinarySearchTree<T> *leftNode;
    BinarySearchTree<T> *rightNode;
    BinarySearchTree<T> *parent;
    T *pData;
    void displayLevelReal(int level) {
        if (level == 0)
            std::cout << *pData << ' ';
        if (leftNode)
            leftNode -> displayLevelReal(level - 1);
        if (rightNode)
            rightNode -> displayLevelReal(level - 1);
    }
};
 
#endif // __BINARY_SEARCH_TREE_H

