#ifndef HEAP_H
#define HEAP_H

#include<iostream>
 
template <typename T>
class Heap
{ 
private:
    T* values;
    int dimVect;
    int capVect;
public:
    Heap(int capVect);
    ~Heap();
 
    int parent(int poz);
 
    int leftSubtree(int poz);
 
    int rightSubtree(int poz);
 
    void pushUp(int poz);
 
    void pushDown(int poz);
 
    void insert(T x);
 
    T peek();
 
    T extractMin();
};



template <typename T>
Heap<T>::Heap(int capVect)
{
    values = new T[capVect];
    dimVect = 0;
    this->capVect = capVect;
}

template <typename T>
Heap<T>::~Heap()
{
    delete values;
}

/* TODO Exercitiul 2 */

template <typename T>
int Heap<T>::parent(int poz)
{
    return (poz-1)/2;
}

template <typename T>
int Heap<T>::leftSubtree(int poz)
{
    return 2*poz+1;
}

template <typename T>
int Heap<T>::rightSubtree(int poz)
{
    return 2*poz+2;
}

template <typename T>
void Heap<T>::pushUp(int poz)
{
    if (poz == 0) return;
    if (values[parent(poz)] <= values[poz]) return;
    T copy = values[parent(poz)];
    values[parent(poz)] = values[poz];
    values[poz] = copy;
    pushUp(parent(poz)); // tail recursion is more elegant than loop?
}

template <typename T>
void Heap<T>::pushDown(int poz)
{
    if (leftSubtree(poz) >= dimVect) return;
    if (rightSubtree(poz) == dimVect &&
        values[poz] <= values[leftSubtree(poz)]) return;
    bool rightIsBigger = values[leftSubtree(poz)] < values[rightSubtree(poz)];
    // bool converts to int as 0 or 1
    // Also leftSubtree(poz) + 1 == rightSubtree(poz)
    if (values[poz] <= values[leftSubtree(poz) + rightIsBigger]) return;
    T copy = values[poz];
    values[poz] = values[leftSubtree(poz) + rightIsBigger];
    values[leftSubtree(poz) + rightIsBigger] = copy;
    // hope common subexpression elimination works well enough
    pushDown(leftSubtree(poz) + rightIsBigger);
}

/* TODO Exercitiul 3 */

template <typename T>
void Heap<T>::insert(T x)
{
    if (dimVect == capVect)
        // fail
        return;
    values[dimVect++] = x;
    pushUp(dimVect-1);
}

template <typename T>
T Heap<T>::peek()
{
    return values[0];
}

template <typename T>
T Heap<T>::extractMin()
{
    T copy = peek();
    if (dimVect > 1)
        values[0] = values[--dimVect];
    else
        dimVect = 0;
    if (dimVect)
        pushDown(0);
    return copy;
}

#endif // HEAP_H
