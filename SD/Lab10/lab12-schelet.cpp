/* Laborator 12 SD - Treapuri
  * Autor: Andrei Parvu - andrei.parvu@cti.pub.ro
  * Echipa SD, 2014
  *
  * Modificari: Mihai Neacsu - mihai.mneacsu@gmail.com
  * Echipa SD, 2015
 */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cassert>

using namespace std;

template <typename T> struct Treap {
  T key;
  int priority;
  Treap<T> *left, *right;
  bool nil;

  // Pentru a rezolva problema 3 trebuie ca fiecare nod sa retina numarul de
  // noduri din subarborle sau
  int nr_nodes;

  // Creaza un nod nil
  Treap() : priority(-1), left(NULL), right(NULL), nil(true), nr_nodes(0) {}

  // Adaugam date, transformand un nod nil intr-un nod obisnuit
  void addData(T key, int priority) {
    this->nil = false;
    this->key = key;
    this->priority = priority;
    this->nr_nodes = 1;
    this->left = new Treap();
    this->right = new Treap();
  }

  // Stergem un nod obisnuit, transformandu-l intr-unul nil
  void delData() {
    this->nil = true;
    this->priority = -1;
    delete this->left;
    delete this->right;
    this->nr_nodes = 0;
  }

  bool isNil() {
    return this->nil;
  }

  bool find(T key) {
    if (this->isNil()) {
      return false;
    }
    if (key == this->key)
        return true;

    if (key < this->key)
        return this->left->find(key);
    else
        return this->right->find(key);

    return false;
  }

  /* Atat insert cat si erase au nevoie de o referinta catre un fatherPointer,
   adica de pointerul left sau right din parinte care pointeaza catre obiectul
   curent. De ce?
   Sa presupunem ca avem urmatoarea configuratie:
                 a
                / \
               b   c
                  / \
                 d   e

   si facem o rotatie dreapta in nodul c. Configuratia care rezulta este:
                 a
                / \
               b   d
                    \
                     c
                      \
                       e

   Dupa cum se poat vedea pointerul right a lui a trebuie sa pointeze in urma
   rotirii catre d. De aceea, o referinta a acelui pointer trebuie pasata catre
   fiul care reprezinta acel pointer, pentru ca nodul a sa vada eventualele
   inlocuiri ce pot aparea in urma unor rotiri.
   Atentie! - desi s-ar putea sa spunem ca putem folosi pointerul this pentru
   a rezolva problema de mai sus, acest lucru este gresit, deoarece this este un
   pointer constant, asupra caruia nu se pot face asignari de forma this = ...
  */

  void rotateRight(Treap<T> *&fatherPointer) {
    // this is the node to rotate, fatherPointer is the parent
    fatherPointer -> left = this -> right; // transfer B
    this -> right = fatherPointer; // transfer z
    fatherPointer = this; // update root
    this->right->nr_nodes = 1 +
        this->right->left->nr_nodes
      + this->right->right->nr_nodes;
    this->nr_nodes = 1 +
        this->left->nr_nodes
      + this->right->nr_nodes;
  }

  void rotateLeft(Treap<T> *&fatherPointer) {
    fatherPointer -> right = this -> left; // transfer B
    this -> left = fatherPointer; // transfer w
    fatherPointer = this; // update root
    this->left->nr_nodes = 1 +
        this->left->left->nr_nodes
      + this->left->right->nr_nodes;
    this->nr_nodes = 1 +
        this->left->nr_nodes
      + this->right->nr_nodes;
  }

  void insert(Treap<T> *&fatherPointer, T key, int priority) {
    if (this->isNil()) {
      this->addData(key, priority);

      return ;
    }

    if (key < this->key) {
        this->left->insert(this->left, key, priority);
    } else {
        this->right->insert(this->right, key, priority);
    }

    this->nr_nodes++;
    assert (fatherPointer == this);
    if (this->left->priority > this->priority) {
        this->left->rotateRight(fatherPointer);
    } else if (this->right->priority > this->priority) {
        this->right->rotateLeft(fatherPointer);
    }
  }

  void erase(Treap<T> *&fatherPointer, T key) {
    if (this->isNil()) {
      return ;
    }

    this->nr_nodes--;

    if (key < this->key) {
        this->left->erase(this->left, key);
    } else if (key > this->key) {
        this->right->erase(this->right, key);
    } else if (this->left->isNil() && this->right->isNil()) {
      this->delData();
    } else {
      if (this->left->priority > this->right->priority) {
        fatherPointer->nr_nodes++; // HACK
        this->left->rotateRight(fatherPointer);
        fatherPointer->erase(fatherPointer, key);
      } else {
        fatherPointer->nr_nodes++; // HACK
        this->right->rotateLeft(fatherPointer);
        fatherPointer->erase(fatherPointer, key);
      }
    }
  }

  void inOrder() {
    if (this->isNil()) {
      return ;
    }
    this->left->inOrder();
    cout << this->key << ' ';
    this->right->inOrder();
  }

  void preOrder(int level = 0) {
    if (this->isNil()) {
      return ;
    }

    for (int i = 0; i < level; i++) {
      cout << ' ';
    }
    cout << this->key << endl;
    this->left->preOrder(level+1);
    this->right->preOrder(level+1);
  }

  T findK(int k) {
    if (nil) return T(); // oh well
    if (this->left->nr_nodes + 1 == k)
        return key;
    if (this->left->nr_nodes + 1 < k)
        return this->right->findK(k - this->left->nr_nodes - 1);
    return this->left->findK(k);
    return 0;
  }
  void badFixNumberNodes() {
    this -> nr_nodes = nil ? 0 : 1;
    if (nil) return;
    this -> left -> badFixNumberNodes();
    this -> nr_nodes += this -> left -> nr_nodes;
    this -> right -> badFixNumberNodes();
    this -> nr_nodes += this -> right -> nr_nodes;
  }
};

int main() {
  srand(time(0));

  Treap<int> *treap = new Treap<int>();

  ifstream f("date.in");

  if (!f) {
    cerr << "Error opening date.in!\n";
    return 0;
  }

  int n, m, x;

  // Verificam functia insert si find
  f >> n;

  for (int i = 1; i <= n; i++) {
    f >> x;

    cout << "Inseram " << x << " in treap\n";

    treap->insert(treap, x, rand() % 1000);
  }

  cout << "\n";

  f >> m;

  for (int i = 1; i <= m; i++) {
    f >> x;

    cout << "Elementul " << x << (treap->find(x) == true ? " se afla " :
         " nu se afla ") << "in treap\n";
  }

  cout << "\n";

  // Verificam delete si find
  f >> n;

  for (int i = 1; i <= n; i++) {
    f >> x;

    cout << "Stergem " << x << " din treap\n";
    treap->erase(treap, x);
  }

  cout << "\n";
  f >> m;

  for (int i = 1; i <= m; i++) {
    f >> x;

    cout << "Elementul " << x << (treap->find(x) == true ? " se afla " :
        " nu se afla ") << "in treap\n";
  }


  cout << "\nCheile sortate\n\n";

  treap->inOrder();

  cout << "\nPrioritatile:\n\n";

  treap->preOrder();

  cout << "\n";

  f >> n;

  for (int i = 1; i <= n; i++) {
    f >> x;

    cout << "Al " << x <<"-lea element din treap este " << treap->findK(x) <<
      "\n";
  }

  return 0;
}

