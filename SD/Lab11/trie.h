#pragma once
#include <string>
class Trie {
    bool present;
    Trie* v[256];
    static int get_pos(char c) {
        return (unsigned char) c;
    }
 public:
    Trie() {
        present = false;
        for (int i = 0; i < 255; i++) v[i] = NULL;
    }
    ~Trie() {
        for (int i = 0; i < 255; i++) delete v[i];
    }
    bool get(std::string s) {
        if (s.length() == 0) return present;
        if (s.length() == 1)
            return v[get_pos(s[0])] && v[get_pos(s[0])]->get("");
        return v[get_pos(s[0])] && v[get_pos(s[0])]->get(s.substr(1));
    }
    void insert(std::string s) {
        if (s.length() == 0)
            present=true; // mark
        else if (s.length() == 1) {
            if (!v[get_pos(s[0])]) v[get_pos(s[0])] = new Trie;
            v[get_pos(s[0])]->insert("");
        }
        else {
            if (!v[get_pos(s[0])]) v[get_pos(s[0])] = new Trie;
            v[get_pos(s[0])]->insert(s.substr(1));
        }
    }
    // this is time-efficient, not space-efficient
    void remove(std::string s) {
        if (s.length() == 0)
            present = false; // unmark
        else if (s.length() == 1)
            {if (v[get_pos(s[0])]) v[get_pos(s[0])]->remove("");}
        else
            if (v[get_pos(s[0])]) v[get_pos(s[0])]->remove(s.substr(1));
    }
};
