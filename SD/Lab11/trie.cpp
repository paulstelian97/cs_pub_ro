#include <iostream>
#include "./trie.h"
using namespace std;
int main() {
    Trie t;
    t.insert("Ana");
    t.insert("Andreea");
    t.insert("Andrei");
    t.insert("Andreiuca");
    cout << boolalpha;
    cout << t.get("Ana") << ' ';
    cout << t.get("Andreia") << ' ';
    cout << t.get("Andreiuca") << ' ';
    t.remove("Andreiuca");
    cout << t.get("Andreiuca") << ' ';
    cout << t.get("Andrei") << endl;
    cout << "Should be: true false true false true" << endl;
}
