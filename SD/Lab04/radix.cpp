#include <iostream>
#include "queue.h"

void radix(unsigned *v, size_t size, unsigned exp) {
	// using base 16
	Queue<unsigned, 256> Q[16];
	for (unsigned i = 0; i < size; i++) {
		unsigned num = v[i]; //is it needed?
		unsigned bucket = num / exp % 16;
		Q[bucket].enqueue(num);
	}
	unsigned i = 0;
	for (int j = 0; j < 16; j++)
		while (!(Q[j].isEmpty()))
			v[i++] = Q[j].dequeue();
}

void fullradix(unsigned *v, size_t size) { for (unsigned exp = 1; exp; exp <<= 4) radix(v, size, exp); }

int main() {
	const size_t N = 50;
	unsigned v[N];
	for (unsigned i = 0; i < N; i++) v[i] = N-i; //have reverse as input
	std::cout << "Initial: ";
	for (unsigned i = 0; i < N; i++) std::cout << v[i] << ' ';
	std::cout << std::endl;
	fullradix(v, N);
	std::cout << "Sorted: ";
	for (unsigned i = 0; i < N; i++) std::cout << v[i] << ' ';
	std::cout << std::endl;
}
