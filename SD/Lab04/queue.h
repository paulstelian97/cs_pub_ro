template <typename T, int N>
class Queue {
    private:
        int head = 0;
        int tail = 0;
        int size = 0;
        T queueArray[N];
 
    public:
        // Constructor
        //Queue() {} //default is alright
 
        // Destructor
        //~Queue() {} //default is alright
 
        // Adauga la coada
        void enqueue(T e) {
            if (size == N) return;
            queueArray[tail] = e;
            tail++;
            if (tail == N) tail = 0;
            size++;
        }
 
        // Extrage din coada
        T dequeue() {
             if (size == 0) return T();
             T result = queueArray[head];
             head++;
             if (head == N) head = 0;
             size--;
             return result;
        }
 
        // Afla primul element
        T front() {
            if (size == 0) return T();
            return queueArray[head];
        }
 
        bool isEmpty() {
            return size == 0;
        }
};
