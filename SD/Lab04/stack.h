#ifndef __STACK__H
#define __STACK__H
 
// Primul argument al template-ului este tipul de date T
// Al doilea argument este dimensiunea maxim a stivei N
template<typename T, int N>
class Stack {
    private:
        // Vectorul de stocare
        T stackArray[N];
 
        // Pozitia in vector a varfului stivei
        int topLevel = 0; //in constructor (not really)
 
    public:
        // Constructor
        //Stack(){} //default is alright
 
        // Destructor
        //~Stack(){} //default is alright
 
        // Operator de adaugare
        void push(T x) {
            if (topLevel < N){
                stackArray[topLevel++] = x;
            }
        }
 
        // Operatorul de stergere
        T pop() {
            if (topLevel > 0)
                return stackArray[--topLevel];
            return T(); //when empty
        }
 
        // Operatorul de consultare
        T peek() {
            if (topLevel == 0) return T();
            return stackArray[topLevel-1];
        }
 
        // Operatorul de verificare dimensiune
        int isEmpty() {
            return topLevel == 0;
        }
};
 
#endif //__STACK__H
