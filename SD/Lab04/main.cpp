#include <iostream>
#include "queue.h"
#include "stack.h"

void testQueue();
void testStack();

int main() {
    std::cout << "Testing queue:" << std::endl;
    testQueue();
    std::cout << std::endl << "Testing stack:" << std::endl;
    testStack();
    std::cout << std::endl << "Done!" << std::endl;
}

void testQueue() {
    Queue<int, 5> q;
    for (int i = 0; i < 10; i++)
        q.enqueue(i);
    std::cout << "Added 0..9 to queue! Now extracting elements...\n";
    while (!q.isEmpty()) std::cout << "Got elem " << q.dequeue() << "\n";
    std::cout << "Filling with 0..9 again!\n";
    for (int i = 0; i < 10; i++) q.enqueue(i);
    std::cout << "Extract two elements then push 10..19...\n";
    q.dequeue(); q.dequeue();
    for (int i = 10; i < 20; i++) q.enqueue(i);
    while (!q.isEmpty()) std::cout << "Got elem " << q.dequeue() << '\n';
    std::cout << "Done working with queue!" << std::endl;
}
void testStack() {
    Stack<int, 5> q;
    for (int i = 0; i < 10; i++)
        q.push(i);
    std::cout << "Added 0..9 to stack! Now extracting elements...\n";
    while (!q.isEmpty()) std::cout << "Got elem " << q.pop() << "\n";
    std::cout << "Filling with 0..9 again!\n";
    for (int i = 0; i < 10; i++) q.push(i);
    std::cout << "Extract two elements then push 10..19...\n";
    q.pop(); q.pop();
    for (int i = 10; i < 20; i++) q.push(i);
    while (!q.isEmpty()) std::cout << "Got elem " << q.pop() << '\n';
    std::cout << "Done working with stack!" << std::endl;
}
