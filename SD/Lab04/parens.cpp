#include <iostream>
#include "stack.h"

bool match(char copen, char cclose) {
	if (copen == '(') return cclose == ')';
	if (copen == '[') return cclose == ']';
	if (copen == '{') return cclose == '}';
	return false; //bad copen
}

int main()
{
	Stack<char, 2500> s; //will be empty
	bool good = true;
	std::string result;
	std::cout << "Test parentheses: ";
	std::getline(std::cin, result);
	for (char c : result)
		switch(c) {
	case '(':
	case '[':
	case '{':
		s.push(c);
		break;
	case ')':
	case ']':
	case '}':
		if (s.isEmpty()) {good = false; break;} //close before open
		if (!match(s.pop(), c)) {good = false; break; } //mismatched close
	//default:
		//nothing
	}
	if (!s.isEmpty()) good=false; //unmatched open
	std::cout << (good ? "Good!" : "Bad!") << std::endl;
}
