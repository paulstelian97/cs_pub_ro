#include <iostream>
#include "singlylinked.h"
using namespace std;
int main() {
	SinglyLinkedList<int> l;
	l.addFirst(5);
	l.addFirst(3);
	l.addLast(4);
	l.addLast(5);
	l.addLast(4);
	l.printList();
	l.removeFirstOccurrence(5);
	l.printList();
	l.removeLastOccurrence(4);
	l.printList();
	cout << "inPlaceReverse" << endl;
	l.inPlaceReverse();
	l.printList();
	while (!l.isEmpty()) {l.removeFirst(); l.printList();}
}
