#include <iostream>
#include "linkedlist.h"
using namespace std;
int main() {
	LinkedList<int> l;
	cout << "addFirst (x2)" << endl;
	l.addFirst(5);
	l.addFirst(3);
	l.printList();
	cout << "addLast (x3)" << endl;
	l.addLast(4);
	l.addLast(5);
	l.addLast(4);
	l.printList();
	cout << "removeFirstOccurence(5)" << endl;
	l.removeFirstOccurrence(5);
	l.printList();
	cout << "removeLastOccurence(4)" << endl;
	l.removeLastOccurrence(4);
	l.printList();
	cout << "reverse" << endl;
	l.reverse();
	l.printList();
	cout << "iteratively remove first" << endl;
	while (!l.isEmpty()) {cout << "removed " << l.removeFirst() << endl; l.printList();}
	cout << "Setting up another list..." << endl;
	l.addFirst(5);
	l.addFirst(3);
	l.addLast(4);
	l.addLast(5);
	l.addLast(6);
	l.addLast(4);
	l.printList();
	cout << "removeDuplicates" << endl;
	l.removeDuplicates();
	l.printList();
}
