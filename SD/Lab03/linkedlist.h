#include <iostream>
template <typename T>
struct Node {
    T value;
    Node<T> *next, *prev;
    Node (T value) {
	this->value = value;
        next = prev = NULL;
    }
    Node() {
        next = prev = NULL; 
    }
};
 
template <typename T>
class LinkedList {
private:
    Node<T> *pFirst, *pLast;
public: 
    // Constructor
    LinkedList() {
		pFirst = pLast = nullptr;
	}
    // Destructor
    ~LinkedList() {
		while (pFirst != nullptr) {
			Node<T>* p = pFirst -> next;
			delete pFirst;
			pFirst = p;
		} // delete all nodes but the last
		// invariant restored; we have a valid list with 1 element
		delete pFirst;
		//pFirst = pLast = nullptr; //not needed since we're gone anyway
	}
 
    /* Adauga un nod cu valoarea == value la inceputul listei. */
    void addFirst(T value) {
		Node<T> *p = new Node<T>(value); //made the node itself; let's link it!
		p -> next = pFirst;
		if (pFirst)
			pFirst -> prev = p;
		p -> next = pFirst;
		pFirst = p;
		if (!pLast) pLast = p;
	}
 
    /* Adauga un nod cu valoarea == value la sfarsitul listei. */
    void addLast(T value) {
		Node<T> *p = new Node<T>(value);
		p -> prev = pLast;
		if (pLast)
			pLast -> next = p;
		p -> prev = pLast;
		pLast = p;
		if (!pFirst) pFirst = p;
	}
 
    /* Elimina elementul de la inceputul listei si intoarce valoarea acestuia. */
    T removeFirst() {
		T value = pFirst -> value;
		Node<T> *p = pFirst -> next;
		delete pFirst;
		pFirst = p;
		if (pFirst)
			pFirst -> prev = nullptr; //let's not break the invariant [valid list], shall we?
		else
			pLast = pFirst;
		return value;
	}
 
    /* Elimina elementul de la sfarsitul listei si intoarce valoarea acestuia. */
    T removeLast() {
		T value = pLast -> value;
		Node<T> *p = pLast -> prev;
		delete pLast;
		pLast = p;
		if (pLast)
			pLast -> next = nullptr;
		else
			pFirst = pLast;
	}
 
    /* Elimina prima aparitie a elementului care are valoarea == value. */
    T removeFirstOccurrence(T value) {
		for (Node<T> *p = pFirst; p /* is not null */; p = p->next)
			if (p -> value == value) {
				// let's remove this node!
				if (p -> prev)
					p -> prev -> next = p -> next;
				else
					pFirst = p -> next;
				if (p -> next)
					p -> next -> prev = p -> prev;
				else
					pLast = p -> prev;
				// done unlinking, now let's free
				delete p;
				if (!pFirst) pLast = pFirst;
				if (!pLast) pFirst = pLast;
				return value; // we didn't lose it on the previous line!
		}
		return value; // very silently fail!
	}
 
    /* Elimina ultima aparitie a elementului care are valoarea == value. */
    T removeLastOccurrence(T value){ //note the near perfect copy-paste!!
		for (Node<T> *p = pLast; p /* is not null */; p = p->prev)
			if (p -> value == value) {
				// let's remove this node!
				if (p -> prev)
					p -> prev -> next = p -> next;
				else
					pFirst = p -> next;
				if (p -> next)
					p -> next -> prev = p -> prev;
				else
					pLast = p -> prev;
				// done unlinking, now let's free
				delete p;
				if (!pFirst) pLast = pFirst;
				if (!pLast) pFirst = pLast;
				return value; // we didn't lose it on the previous line!
		}
		return value; // very silently fail!
	}
 
    /* Afiseaza elementele listei pe o singura linie, separate printr-un spatiu. */
    void printList() {
		if (!pFirst) return;
		for (Node<T> *p = pFirst; p != pLast; p = p->next)
			std::cout << p -> value << ' ';
		if (pLast) std::cout << pLast -> value << std::endl; // no line on empty
	}
		
    /* Intoarce true daca lista este vida, false altfel. */
    bool isEmpty() { return pFirst == nullptr; }
	void removeDuplicates() {
		for (Node<T> *p1 = pFirst; p1; p1 = p1->next)
			for (Node<T> *p2 = p1 -> next; p2; p2 = p2->next)
				if (p1 -> value == p2 -> value) {
					//let's remove p2
					if (p2->prev) p2->prev->next = p2->next;
					else pFirst = p2->next;
					if (p2->next) p2->next->prev = p2->prev;
					else pLast = p2->prev;
					Node<T> *tmp = p2->prev;
					delete p2;
					p2 = tmp;
		}
	}
	void reverse() {
		for (Node<T> *p = pFirst; p; p = p->prev) {
			Node<T> *q = p->next;
			p->next = p->prev;
			p->prev = q;
		}
		Node<T> *p = pFirst;
		pFirst = pLast;
		pLast = p;
	}
};
