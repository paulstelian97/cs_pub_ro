#include <iostream>
template <typename T>
class ArrayList {
private:
    T *array;
    int size, capacity;
	const int initsize = 1<<9, increment = 1<<9; //512 elements
	bool increaseCapacity() {
		T *newArray = new T[size + capacity];
		if (!newArray) return false;
		for (int i = 0; i < size; i++) newArray[i] = array[i];
		delete[] array;
		array = newArray;
		return true;
	}
public: 
    // Constructor
    ArrayList() {
		size = 0;
		capacity = initsize;
		array = new T[capacity];
	}
    // Destructor
    ~ArrayList() {
		delete[] array; //Simple as that, other fields deconstruct automatically
	}
 
    /* Adauga un nod cu valoarea == value la inceputul vectorului. */
    void addFirst(T value) {
		if (size == capacity && !increaseCapacity()) return; //silently fail
		for (int i = size; i > 0; i--) array[i] = array[i-1];
		array[0] = value;
		size++;
	}
 
    /* Adauga un nod cu valoarea == value la sfarsitul vectorului. */
    void addLast(T value) {
		if (size == capacity && !increaseCapacity()) return;
		array[size] = value;
		size++;
	}
 
    /* Elimina elementul de la inceputul vectorului si intoarce valoarea acestuia. */
    T removeFirst() {
		T copy = array[0];
		for (int i = 1; i < size; i++) array[i-1] = array[i];
		size--;
		return copy;
	}
 
    /* Elimina elementul de la sfarsitul vectorului si intoarce valoarea acestuia. */
    T removeLast() {
		size--;
		return array[size];
	}
 
    /* Elimina prima aparitie a elementului care are valoarea == value. */
    T removeFirstOccurrence(T value) {
		for (int i = 0; i < size; i++) 
			if (array[i] == value) {
				for (int j = i+1; j < size; j++) array[j-1] = array[j];
				size--;
				return value;
		}
		return value; //silently fail
	}
 
    /* Elimina ultima aparitie a elementului care are valoarea == value. */
    T removeLastOccurrence(T value){
		for (int i = size-1; i >= 0; i--) 
			if (array[i] == value) {
				for (int j = i+1; j < size; j++) array[j-1] = array[j];
				size--;
				return value;
		}
		return value; //silently fail
	}
 
    /* Afiseaza elementele vectorului pe o singura linie, separate printr-un spatiu. */
    void printList() {
		for (int i = 0; i < size-1; i++)
			std::cout << array[i] << ' ';
		if (size)
			std::cout << array[size-1] << std::endl;
	}
 
    /* Intoarce true daca vectorul este gol, false altfel. */
    bool isEmpty() {return !size;}
	void reverse() {
		for (int i = 0; 2*i < size; i++)
			std::swap(array[i], array[size-i-1]);
	}
};
