#include <iostream>
template <typename T>
struct Node {
    T value;
    Node<T> *next;
    Node (T value) {
	this->value = value;
        next = NULL;
    }
    Node() {
        next = NULL; 
    }
};
 
template <typename T>
class SinglyLinkedList {
private:
    Node<T> *pFirst, *pLast;
public: 
    // Constructor
    SinglyLinkedList() {
		pFirst = pLast = nullptr;
	}
    // Destructor
    ~SinglyLinkedList() {
		while (pFirst != nullptr) {
			Node<T>* p = pFirst -> next;
			delete pFirst;
			pFirst = p;
		} // delete all nodes but the last
		// invariant restored; we have a valid list with 1 element
		delete pFirst;
		//pFirst = pLast = nullptr; //not needed since we're gone anyway
	}
 
    /* Adauga un nod cu valoarea == value la inceputul listei. */
    void addFirst(T value) {
		Node<T> *p = new Node<T>(value); //made the node itself; let's link it!
		p -> next = pFirst;
		p -> next = pFirst;
		pFirst = p;
		if (!pLast) pLast = p;
	}
 
    /* Adauga un nod cu valoarea == value la sfarsitul listei. */
    void addLast(T value) {
		Node<T> *p = new Node<T>(value);
		if (pLast)
			pLast -> next = p;
		pLast = p;
		if (!pFirst) pFirst = p;
	}
 
    /* Elimina elementul de la inceputul listei si intoarce valoarea acestuia. */
    T removeFirst() {
		T value = pFirst -> value;
		Node<T> *p = pFirst -> next;
		delete pFirst;
		pFirst = p;
		if (!pFirst) pLast = nullptr;
		return value;
	}
 
    /* Elimina elementul de la sfarsitul listei si intoarce valoarea acestuia. */
    T removeLast() {
		T value = pLast -> value;
// have to find second last manually
		if (!pFirst -> next) {
			delete pFirst;
			pFirst = pLast = nullptr;
			return value;
		}
		Node<T> *p;
		for (p = pFirst; p -> next != pLast; p = p -> next) /* just iterate! */;
		// p is second last now
		p -> next = nullptr;
		delete pLast;
		pLast = p;
	}
 
    /* Elimina prima aparitie a elementului care are valoarea == value. */
    T removeFirstOccurrence(T value) {
		if (pFirst -> value == value) {
			Node<T> *p = pFirst -> next;
			delete pFirst;
			pFirst = p;
			if (!pFirst) pLast = nullptr;
			return value;
		}
		// Beware: Dragons below
		for (Node<T> *p = pFirst; p->next /* is not null */; p = p->next)
			if (p -> next -> value == value) {
				// let's remove this node!
				if (!p -> next -> next)
					pLast = p;
				Node<T> *q = p -> next -> next;
				delete p -> next;
				p -> next = q;
				return value; // we didn't lose it on the previous line!
		}
		// Beware: Dragons above
		return value; // very silently fail!
	}
 
    /* Elimina ultima aparitie a elementului care are valoarea == value. */
    T removeLastOccurrence(T value) {
		Node<T> *p, *q = nullptr;
		for (p = pFirst; p != pLast; p = p -> next)
			if (p -> value == value) q = p;
		if (q == pFirst) return removeFirst(), value; // The comma operator!!
		if (!q) return value; // very silently fail!
		for (p = pFirst; p -> next != q; p = p -> next) /* just iterate */;
		p -> next = q -> next; //unlink
		delete q; //and remove
		return value;
	}
    /* Afiseaza elementele listei pe o singura linie, separate printr-un spatiu. */
    void printList() {
		if (!pFirst) return;
		for (Node<T> *p = pFirst; p != pLast; p = p->next)
			std::cout << p -> value << ' ';
		if (pFirst) std::cout << pLast -> value << std::endl; // no line on empty
	}
		
    /* Intoarce true daca lista este vida, false altfel. */
    bool isEmpty() { return pFirst == nullptr; }
	void inPlaceReverse() {
		if (pFirst == pLast) return; //lists of up to 1 elements don't need rev
		Node<T> *p, *q = nullptr;
		for (p = pFirst; p; /* next is too complex */) {
			Node<T> *tmp = p->next; // backup next
			p -> next = q; // let current point to prev
			q = p; // let current become prev
			p = tmp; // and advance
		}
		p = pFirst;
		pFirst = pLast;
		pLast = p;
	}
};
