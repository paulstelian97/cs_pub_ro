#include <cstring>
#include <iostream>
class Persoana{
	char* nume;
	int bani;
	Persoana() {nume=NULL;bani=0;} //let's not allow it go public!
public:
	Persoana(const Persoana& p) {
		this->nume = new char[std::strlen(p.nume) + 1];
		std::strcpy(this->nume, p.nume);
		this->bani = p.bani;
	}
	Persoana(Persoana&& p) { //move constructor
		this->nume = p.nume;
		this->bani = p.bani;
		p.nume = NULL;
		//p will no longer be destructed
	}
	Persoana(char* nume, int bani) {
		this->nume = new char[std::strlen(nume) + 1];
		std::strcpy(this->nume, nume);
		this->bani = bani;
	}
	void operator=(const Persoana& other) {
		delete[] this->nume;
		this->nume = new char[std::strlen(other.nume) + 1];
		std::strcpy(this->nume, other.nume);
		this->bani = other.bani;
	}
	void operator=(Persoana&& other) {
		delete[] this->nume;
		this->nume = other.nume;
		other.nume = NULL;
		this->bani = other.bani;
	}
	char const * const getNume() const {return this->nume;}
	int getBani() const {return this->bani;}
	Persoana& operator--() {
		this->bani--;
		return *this;
	}
	Persoana operator+(const Persoana& other) const {
		Persoana result;
		result.nume = new char[strlen(this->nume) + strlen(other.nume) + 3];
		std::strcpy(result.nume, this->nume);
		std::strcat(result.nume, ", ");
		std::strcat(result.nume, other.nume);
		result.bani = this->bani + other.bani;
		return result;
	}
	Persoana operator--(int) {
		Persoana other = *this; //copy
		--(*this); //decrement this one
		return other;
	}
	~Persoana() {
		if (this->nume)
		delete[] this->nume;
	}
};
int main() {
	Persoana a("Mihai", 5), b("Andrei", 12);
	Persoana c = a;
	std::cout << c.getNume() << " " << c.getBani() << std::endl;
	c = b;
	std::cout << c.getNume() << " " << c.getBani() << std::endl;
	c = a+b;
	std::cout << c.getNume() << " " << c.getBani() << std::endl;
	std::cout << (c--).getBani() << std::endl;
	std::cout << c.getNume() << " " << c.getBani() << std::endl;
	std::cout << (--c).getBani() << std::endl;
	std::cout << c.getNume() << " " << c.getBani() << std::endl;
}
