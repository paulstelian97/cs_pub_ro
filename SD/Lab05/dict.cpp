// dict.cpp
#include <iostream>
#include <string>
#include <sstream>
#include "hashtable.h"
using namespace std;
unsigned int strhsh(string s) {
	unsigned int result = 2531011;
	for (char c : s) {
		result += c;
		result *= 214013; //overflows are well defined on unsigned
	}
	return result;
}
void myget(Hashtable<string, string>& ht, const string& comm);
void myput(Hashtable<string, string>& ht, const string& comm);
int main() {
	string s;
	Hashtable<string, string> ht(200, strhsh);
	while(true) {
		cout << "Enter command: ";
		getline(cin, s);
		if (s.substr(0, 4) == "get ")
			myget(ht, s.substr(4));
		else if (s.substr(0, 4) == "put ")
			myput(ht, s.substr(4));
		else if (s.substr(0, 5) == "quit ")
			break;
		else
			cout << "Command not recognized." << endl;
	}
}

void myput(Hashtable<string, string>& ht, const string& comm) {
	// let's extract the word, without quotes
	istringstream in(comm);
	string word, def;
	in >> word;
	word = word.substr(1, word.length()-2); //cut the double quotes out
	getline(in, def);
	def = def.substr(2, def.length()-3);
	ht.put(word, def);
	cout << word << " - adăugat cu succes" << endl;
}
void myget(Hashtable<string, string>& ht, const string& comm) {
	string word = comm.substr(1, comm.length()-2);
	cout << word << " - ";
	if (ht.has_key(word))
		cout << ht.get(word) << endl;
	else
		cout << "cuvânt inexistent!" << endl;
}
