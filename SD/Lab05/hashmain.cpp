// hashmain.cpp
#include <iostream>
#include <string>
#include "hashtable.h"
using namespace std;
unsigned int strhsh(string s) {
	unsigned int result = 2531011;
	for (char c : s) {
		result += c;
		result *= 214013; //overflows are well defined
	}
	return result;
}
int main() {
	string s;
	Hashtable<string, int> ht(20, strhsh);
	while(true) {
		cout << "Enter string: ";
		getline(cin, s);
		cout << "String is \"" << s << "\", pos is ";
		cout << strhsh(s) % 20 << "...";
		cout << std::flush;
		if (ht.has_key(s)) {
			int oldval = ht.get(s);
			ht.put(s, oldval + 1);
		} else {
			ht.put(s, 1);
		}
		cout << " incremented, new value is " << ht.get(s) << endl;
	}
}
