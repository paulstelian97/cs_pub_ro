#ifndef __HASHTABLE__H
#define __HASHTABLE__H
#include <list>
 
template<typename Tkey, typename Tvalue>
struct elem_info {
    Tkey key = Tkey();
    Tvalue value = Tvalue();
};
 
template<typename Tkey, typename Tvalue>
class Hashtable {
private:
    std::list<struct elem_info<Tkey, Tvalue> > *H;
    int HMAX;
    unsigned int (*hash) (Tkey);
    unsigned int size = 0;
 
public:
    const bool resizes = true;
    const int& getHMAX() {return HMAX;}
    Hashtable(int hmax, unsigned int (*h)(Tkey)) : HMAX(hmax), hash(h) {
        H = new std::list<struct elem_info<Tkey, Tvalue> >[hmax];
    }
    ~Hashtable() {
        delete[] H;
    }

    Hashtable(Hashtable& other) = delete; //let's not allow copying
    Hashtable(Hashtable&& other) = delete; //nor moving
    Hashtable& operator=(const Hashtable&) = delete; //nor assignments
    Hashtable& operator=(const Hashtable&&) = delete;

    void resize() {
        std::list<elem_info<Tkey, Tvalue> > *v;
        auto newsize = HMAX * 2;
        v = new std::list<elem_info<Tkey, Tvalue> >[newsize];
        if (!v) return; //resize failed silently; no performance improvement
        // Now let's copy the elements from the lists
        for (int i = 0; i < HMAX; i++)
            // each of the lists shall be iterated
            while (!H[i].empty()) {
                auto& elem = H[i].front();
                v[hash(elem.key) % newsize].push_back(elem);
                H[i].pop_front();
            }
        // Now a bit of cleanup
        delete[] H;
        H = v;
        HMAX = newsize;
    }

    void put(Tkey key, Tvalue value) {
        auto& bucket = H[hash(key) % HMAX];
        for (auto& elem : bucket) {
            if (elem.key == key) { //if found
                elem.value = value;
                return;
            }
        }
        struct elem_info<Tkey, Tvalue> elem;
        elem.key = key;
        elem.value = value;
        bucket.push_back(elem);
        size++;
        if (size > HMAX * 0.75) resize(); //resize optimization
    }
    void remove(Tkey key)
    {
        auto old_size = H[hash(key) % HMAX].length();
        H[hash(key) % HMAX].remove_if(
            [key] //need to capture parameter
            (struct elem_info<Tkey, Tvalue> elem)
            { return elem.key == key; } //only on key match
        );
        if (old_size == H[hash(key) % HMAX].length())
            return; // nothing removed
        size--;
    }
    Tvalue get(Tkey key)
    {
        for (auto& elem : H[hash(key) % HMAX])
            if (elem.key == key)
                return elem.value;
        return Tvalue(); //UB if element not found
    }
    bool has_key(Tkey key)
    {
        for (auto& elem : H[hash(key) % HMAX])
            if (elem.key == key)
                return true;
        return false;
    }
};
 
#endif //__HASHTABLE__H
