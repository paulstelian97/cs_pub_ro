#ifndef __HASHTABLE__H
#define __HASHTABLE__H
#include <list>
 
template<typename Tkey, typename Tvalue>
struct elem_info {
    Tkey key = Tkey();
    Tvalue value = Tvalue();
};
 
template<typename Tkey, typename Tvalue>
class Hashtable {
private:
    std::list<struct elem_info<Tkey, Tvalue> > *H;
    int HMAX;
    unsigned int (*hash) (Tkey);
 
public:
    const bool resizes = false;
    const int& getHMAX() { return HMAX; }
    Hashtable(int hmax, unsigned int (*h)(Tkey)) : HMAX(hmax), hash(h) {
        H = new std::list<struct elem_info<Tkey, Tvalue> >[hmax];
    }
    ~Hashtable() {
        delete[] H;
    }

    Hashtable(Hashtable& other) = delete; //let's not allow copying
    Hashtable(Hashtable&& other) = delete; //nor moving
    Hashtable& operator=(const Hashtable&) = delete; //nor assignments
    Hashtable& operator=(const Hashtable&&) = delete;

    void put(Tkey key, Tvalue value) {
        auto& bucket = H[hash(key) % HMAX];
        for (auto& elem : bucket) {
            if (elem.key == key) { //if found
                elem.value = value;
                return;
            }
        }
        struct elem_info<Tkey, Tvalue> elem;
        elem.key = key;
        elem.value = value;
        bucket.push_back(elem);
    }
    void remove(Tkey key)
    {
        H[hash(key) % HMAX].remove_if(
            [key] //need to capture parameter
            (struct elem_info<Tkey, Tvalue> elem)
            { return elem.key == key; } //only on key match
        );
    }
    Tvalue get(Tkey key)
    {
        for (auto& elem : H[hash(key) % HMAX])
            if (elem.key == key)
                return elem.value;
        return Tvalue(); //UB if element not found
    }
    bool has_key(Tkey key)
    {
        for (auto& elem : H[hash(key) % HMAX])
            if (elem.key == key)
                return true;
        return false;
    }
};
 
#endif //__HASHTABLE__H
