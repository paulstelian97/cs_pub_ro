#include <iostream>
#include "rhashtable.h"
using namespace std;
unsigned int ihsh(int x) {return x;}
int main() {
    Hashtable<int, int> ht(15, ihsh);
    for (int i = 0; i < 25; i++) {
        cout << "Adding " << i << "..." << flush;
        ht.put(i, 1);
        cout << "Done! ";
	cout << "Internal HMAX: " << ht.getHMAX() << endl;
    }
}
