#ifndef __GRAPH__H
#define __GRAPH__H
 
#include <vector>
// Structura Node este utila doar pentru implementarea cu liste de vecini
//struct Node {
//	std::vector<int> neighbors;
//};
 
class Graph {
	private:
		// std::vector<Node> nodes; // Implementare cu liste de vecini
		int **adiacency_matrix;  // Implementare cu matrice de adiacenta
		Graph(const Graph& other) {} // disable copy
		Graph& operator=(const Graph& other) = delete; //disable copy
		int size;
	public:
		Graph(int size) {
			adiacency_matrix = new int*[size];
			for (int i = 0; i < size; i++)
				adiacency_matrix[i] = new int[size];
			for (int i = 0; i < size; i++)
				for (int j = 0; j < size; j++)
					adiacency_matrix[i][j] = 0;
			this->size = size;
		}
		~Graph() {
			for (int i = 0; i < size; i++)
				delete[] adiacency_matrix[i];
			delete[] adiacency_matrix;
		}
 
		void add_node(int node) {}    // Atentie: pentru implementarea cu matrice
		void remove_node(int node) {} // puteti ignora aceaste doua functii
 
		void add_edge(int src, int dst) { adiacency_matrix[src][dst]=1;}
		void remove_edge(int src, int dst) {adiacency_matrix[src][dst]=0;}
		bool has_edge(int src, int dst) {return adiacency_matrix[src][dst];}
 
		std::vector<int> get_neighbors(int node) {
			std::vector<int> v;
			for (int i = 0; i < size; i++)
				if (has_edge(node, i))
					v.push_back(i);
			return v;
		}
};
 
#endif //__GRAPH__H

