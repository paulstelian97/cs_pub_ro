#include <iostream>
#include "bfs.h"
#include "graph.h"
using namespace std;
int main() {
	Graph g(5);
	g.add_edge(0,1);
	g.add_edge(1,0);
	g.add_edge(0,2);
	g.add_edge(2,0);
	g.add_edge(1,3);
	g.add_edge(3,1);
	g.add_edge(2,3);
	g.add_edge(3,2);
	const auto& v1 = bfs(g, 5);
	const auto& v2 = dfs(g, 5);
	cout << "BFS: ";
	for (const auto& i : v1) cout << i << " ";
	cout << "\nDFS: ";
	for (const auto& i : v2) cout << i << " ";
	cout << endl;
}
