#include <vector>
#include <queue>
#include <stack>
#include <bitset>
#include <cassert>
#include "graph.h"
bool all(std::vector<bool> v) {
	for (bool b : v) if (!b) return false;
	return true;
}
bool any(std::vector<bool> v) {
	for (bool b : v) if (b) return true;
	return false;
}
std::vector<int> bfs(Graph& g, int size) {
	std::vector<int> result;
	std::vector<bool> v(size);
	std::queue<int> t;
	//for (int i = 0; i < size; i++) v[i] = false; //not needed really
	assert (!any(v));
	while (!all(v)) {
		int i;
		for (i = 0; i < size && v[i]; i++)
			; // do nothing, just advance i to the first false
		t.push(i); //start search from first not covered
		while (!t.empty()) {
			int i = t.front();
			t.pop();
			//if (v[i]) continue; //skip already seen
			if (!v[i])
				result.push_back(i); //send to result
			v[i] = true; //mark as seen
			for (int j = 0; j < size; j++)
				if (g.has_edge(i, j) && !v[j])
					t.push(j);
		}
	}
	return result;
}
std::vector<int> dfs(Graph& g, int size) {
	std::vector<int> result;
	std::vector<bool> v(size);
	std::stack<int> t;
	//for (int i = 0; i < size; i++) v[i] = false; //not needed really
	assert (!any(v));
	while (!all(v)) {
		int i;
		for (i = 0; i < size && v[i]; i++)
			; // do nothing, just advance i to the first false
		t.push(i); //start search from first not covered
		while (!t.empty()) {
			int i = t.top();
			t.pop();
			//if (v[i]) continue; //skip already seen
			if (!v[i])
				result.push_back(i); //send to result
			v[i] = true; //mark as seen
			for (int j = size-1; j >= 0; j--)
				if (g.has_edge(i, j) && !v[j])
					t.push(j);
		}
	}
	return result;
}
